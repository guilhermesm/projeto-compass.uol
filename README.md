# Projeto Compass.UOL
![Compass banner](https://d1.awsstatic.com/logos/Amazon%20MSK%20logos/COMPASS-LOGO.68deff1f584bea0767845872fa8f23b927d25149.png)

Olá! Este será um repositório de conteúdos abordados durante a trilhagem de estudos do Programa de Bolsas [Compass.UOL](compass.uol/) - Automação de Testes em Java Backend, com o começo em 22/05/2023 e duração de 3 meses. 

Será dividido em 6 Sprints, com a duração de 15 dias cada. Ao final de cada Sprint será feito uma Challenge, que é uma reunião mais longa com o objetivo de apresentar um resumo do Sprint que passou. <br>
- [Sprint 1](https://gitlab.com/guilhermesm/projeto-compass.uol/-/tree/pb_sprint1?ref_type=heads) - Processos Ágeis e Fundamentos de Teste
- [Sprint 2](https://gitlab.com/guilhermesm/projeto-compass.uol/-/tree/pb_sprint2?ref_type=heads) - Análise e Testes de APIs REST
- [Sprint 3](https://gitlab.com/guilhermesm/projeto-compass.uol/-/tree/pb_sprint3?ref_type=heads) - Automação de Testes com RestAssured
- [Sprint 4](https://gitlab.com/guilhermesm/projeto-compass.uol/-/tree/pb_sprint4?ref_type=heads) - DoD, DoR e boas práticas com RestAssured
- [Sprint 5](https://gitlab.com/guilhermesm/projeto-compass.uol/-/tree/pb_sprint5?ref_type=heads) - RestAssured++
- [Sprint 6](https://gitlab.com/guilhermesm/compass-challenge-final) - Docker, CI e Challenge Final

* Imagem ilustrativa que mostra todo caminho percorrido.
<div align="center">
    <img src="img/trilha-compass.jpg" alt="trilha-compass" width="670px" height="490px">
</div>



